INSERT INTO users(username, password, email, enabled) values('jigarsnaik', '{noop}password', 'jigarsnaik@gmail.com', true);
INSERT INTO users(username, password, email, enabled) values('user', '{noop}password', 'jigarsnaik@gmail.com', true);
INSERT INTO users(username, password, email, enabled) values('jagrutijigarnaik', '{noop}password', 'jagrutijigarnaik@gmail.com', true);
INSERT INTO users(username, password, email, enabled) values('admin', '{noop}password', 'admin@gmail.com', true);

INSERT INTO authorities values('jigarsnaik', 'USER');
INSERT INTO authorities values('jagrutijigarnaik', 'USER');
INSERT INTO authorities values('admin', 'ADMIN');
INSERT INTO authorities values('user', 'USER');
